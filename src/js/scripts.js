
@@include('js/modules/materialize.min.js')


jQuery(document).ready(function () {
	AOS.init({
		duration: 1000,
		offset: 0,
		easing: 'ease',
	});
	
	jQuery('.modal').modal();

  jQuery('.tabs').tabs({
  duration : 3000
});
   /* Pause Video*/
	jQuery(document).on("click", "div.modal-overlay , a.modal-close ", function(e) {
		jQuery('div.video-popup iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
	});

	/* Stop Video Will Restart From Starting */
   jQuery(document).on("click", "div.modal-overlay , a.modal-close ", function(e) {     
        jQuery('div.video-popup iframe').attr('src', jQuery('div.video-popup iframe').attr('src'));
    });


   jQuery(".menu-icon").click(function(){
 			 jQuery("body").toggleClass("open");
		});



   /*Equal Height Box*/
	// Select and loop the container element of the elements you want to equalise
    jQuery('.three-column-boxes').each(function(){  
      
      // Cache the highest
      var highestBox = 0;
      
      // Select and loop the elements you want to equalise
      jQuery('.col .column-box', this).each(function(){
        
        // If this box is higher than the cached highest then store it
        if(jQuery(this).height() > highestBox) {
          highestBox = jQuery(this).height(); 
        }
      
      });  
            
      // Set the height of all those children to whichever was highest 
      jQuery('.col .column-box',this).height(highestBox);
                    
    }); 
});

jQuery(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();

    if (scroll >= 100) {
        jQuery("header").addClass("darkHeader");
    } else {
        jQuery("header").removeClass("darkHeader");
    }
});




