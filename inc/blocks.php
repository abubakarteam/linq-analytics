<?php 
/**
 * Functions for custom Gutenberg blocks
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Time with God Theme
 * @since 1.0.0
 */

/**
 * Register custom Gutenberg custom blocks category
 */
function custom_acf_block_categories( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'custom-blocks',
				'title' => __( 'Glide Blocks', 'linq-analytics' ),
				'icon'  => 'admin-generic',
			),
		)
	);
}
add_filter( 'block_categories', 'custom_acf_block_categories', 10, 2 );

/**
 * Register custom Gutenberg blocks
 */
add_action( 'acf/init', 'theme_acf_init' );
function theme_acf_init() {

	if ( function_exists( 'acf_register_block' ) ) {
		// register a Hero Banner Block
		acf_register_block(
			array(
				'name'            => 'hero-banner-section',
				'title'           => __( 'Hero Banner Block', 'linq-analytics' ),
				'description'     => __( 'A Hero Banner Section Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Hero Banner Section', 'Banner Two Column Block' ),
			)
		);
		// register a client connected Block 
		acf_register_block(
			array(
				'name'            => 'client-connected-section',
				'title'           => __( 'Client Connected Block', 'linq-analytics' ),
				'description'     => __( 'A Client Connected Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Client Connected Section', 'Client Connected Block' ),
			)
		);
		// register a Build Your Source Block 
		acf_register_block(
			array(
				'name'            => 'build-your-source-section',
				'title'           => __( 'Build Your Source Block', 'linq-analytics' ),
				'description'     => __( 'A Build Your Source Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Build Your Source Section', 'Build Your Source Block' ),
			)
		);
		// register a Video Block 
		acf_register_block(
			array(
				'name'            => 'video-section',
				'title'           => __( 'Video Block', 'linq-analytics' ),
				'description'     => __( 'A Video Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Video Section', 'Video Block' ),
			)
		);
		// register a three column Block 
		acf_register_block(
			array(
				'name'            => 'three-column-section',
				'title'           => __( 'Three Column Block', 'linq-analytics' ),
				'description'     => __( 'A Three Column with Image and Content Editable Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Three Column Section', 'Three Column Block' ),
			)
		);
		// register a content with cta Block 
		acf_register_block(
			array(
				'name'            => 'content-with-cta-section',
				'title'           => __( 'Content With CTA Block', 'linq-analytics' ),
				'description'     => __( 'A block to add content with cta.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Content with cta Section', 'Content with cta Block' ),
			)
		);
		// register a testimonial Block 
		acf_register_block(
			array(
				'name'            => 'testimonial-section',
				'title'           => __( 'Testimonial Block', 'linq-analytics' ),
				'description'     => __( 'A block to show testimonial.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Testimonial Section', 'Testimonial Block' ),
			)
		);
		// register a two column with form Block 
		acf_register_block(
			array(
				'name'            => 'two-column-with-form-section',
				'title'           => __( 'Two Column with Form Block', 'linq-analytics' ),
				'description'     => __( 'A two column with form block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Two column block Section', 'Two column with form Block' ),
			)
		);
		// register a tabbing Block 
		acf_register_block(
			array(
				'name'            => 'tabbing-section',
				'title'           => __( 'Tabbing Block', 'linq-analytics' ),
				'description'     => __( 'A tabbing block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Tabbing Section', 'Tabbing Block' ),
			)
		);
		// register a content with cta Block 
		acf_register_block(
			array(
				'name'            => 'heading-content-section',
				'title'           => __( 'Heading with content block', 'linq-analytics' ),
				'description'     => __( 'A block to add heading and content.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Heading content Section', 'Heading with content Block' ),
			)
		);
		// register a stats Block 
		acf_register_block(
			array(
				'name'            => 'stats-section',
				'title'           => __( 'Stats block', 'linq-analytics' ),
				'description'     => __( 'A block stats.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Stats Section', 'Stats Block' ),
			)
		);
		// register a stats Block 
		acf_register_block(
			array(
				'name'            => 'logo-with-info-section',
				'title'           => __( 'Logo with Info block', 'linq-analytics' ),
				'description'     => __( 'A block for logo info section.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Logo Section', 'Logo with Info Block' ),
			)
		);
		// register a stats Block 
		acf_register_block(
			array(
				'name'            => 'faq-section',
				'title'           => __( 'Faq block', 'linq-analytics' ),
				'description'     => __( 'A block faq section.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Faq Section', 'Faq Block' ),
			)
		);
		// register a image content Block
		acf_register_block(
			array(
				'name'            => 'image-content-section',
				'title'           => __( 'Image Content Block', 'linq-analytics' ),
				'description'     => __( 'A block for Image and content section.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Image Content Section', 'Faq Block' ),
			)
		);
		// register a team Block
		acf_register_block(
			array(
				'name'            => 'team-section',
				'title'           => __( 'Team Block', 'linq-analytics' ),
				'description'     => __( 'A block for team members and their info.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Team Section', 'Team Member Block' ),
			)
		);
		// register a toolkit banner Block
		acf_register_block(
			array(
				'name'            => 'toolkit-banner-section',
				'title'           => __( 'Toolkit Banner Block', 'linq-analytics' ),
				'description'     => __( 'A Toolkit Banner Section Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Toolkit Banner Section', 'Toolkit Block' ),
			)
		);
		// register a logo list Block
		acf_register_block(
			array(
				'name'            => 'logo-list-section',
				'title'           => __( 'Logo List Block', 'linq-analytics' ),
				'description'     => __( 'A Logo List Section Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Logo List Section', 'Logo List Block' ),
			)
		);
		// register a table Block
		acf_register_block(
			array(
				'name'            => 'table-section',
				'title'           => __( 'Table Block', 'linq-analytics' ),
				'description'     => __( 'A Table Section Block.', 'linq-analytics' ),
				'render_callback' => 'custom_acf_block_render_callback',
				'category'        => 'custom-blocks',
				'icon'            => 'edit',
				'mode'            => 'edit',
				'keywords'        => array( 'Table Section', 'Table Block' ),
			)
		);
	}
}

/**
 * Reder custom Gutenberg blocks
 */

function custom_acf_block_render_callback( $block ) {
	// convert name ("acf/Testimonial") into path friendly slug ("Testimonial")
	$slug = str_replace( 'acf/', '', $block['name'] );
	
	// include a template part from within the "block" folder
	if ( file_exists( get_theme_file_path( "/blocks/block-{$slug}.php" ) ) ) {
		include get_template_directory()."/blocks/block-{$slug}.php";
	}
}