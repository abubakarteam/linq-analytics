<?php
/* Theme Option */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}
/* End Theme Option */

/*CPT Testimonial*/
function Testimonial_custom_post_type() {

    $labels = array(
        'name'                => _x( 'Testimonials', 'linq-analytics' ),
        'singular_name'       => _x( 'Testimonial', 'linq-analytics' ),
        'menu_name'           => __( 'Testimonials', 'linq-analytics' ),
        'parent_item_colon'   => __( 'Parent Testimonial', 'linq-analytics' ),
        'all_items'           => __( 'All Testimonials', 'linq-analytics' ),
        'view_item'           => __( 'View Testimonial', 'linq-analytics' ),
        'add_new_item'        => __( 'Add New Testimonial', 'linq-analytics' ),
        'add_new'             => __( 'Add New', 'linq-analytics' ),
        'edit_item'           => __( 'Edit Testimonial', 'linq-analytics' ),
        'update_item'         => __( 'Update Testimonial', 'linq-analytics' ),
        'search_items'        => __( 'Search Testimonial', 'linq-analytics' ),
        'not_found'           => __( 'Not Found', 'linq-analytics' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'linq-analytics' ),
    );
     
    $args = array(
        'label'               => __( 'Testimonials', 'linq-analytics' ),
        'description'         => __( 'Testimonial news and reviews', 'linq-analytics' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-testimonial',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );

    register_post_type( 'testimonials', $args );
 
}
 
add_action( 'init', 'Testimonial_custom_post_type', 0 );

/**Testimonial display shortcode****/
add_shortcode('testimonial_display', 'testimonial_display');
function testimonial_display(){
    ob_start();
    $args = array(
        'post_type' => 'testimonials',
        'post_status' => 'publish',
        'posts_per_page' => 1, 
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $loop = new WP_Query( $args ); 
    if($loop->have_posts()):
        while($loop->have_posts()): $loop->the_post();
            $testimonial_content = get_field('content', get_the_ID());
            $testimonial_logo = get_field('logo', get_the_ID());
            $company_name = get_field('company_name', get_the_ID());
           ?>

                <div class="quote-text"><?php echo $testimonial_content; ?></div>
                <div class="quote-footer tbl">
                   <div class="quote-author tbl-col"><?php echo get_the_title(); ?></div>
                   <?php if(!empty($testimonial_logo)): ?>
                            <div class="quote-logo tbl-col"><img src="<?php echo $testimonial_logo; ?>"></div>
                    <?php elseif(!empty($company_name)): ?>   
                            <div class="quote-company tbl-col"><?php echo $company_name; ?></div> 
                   <?php endif; ?>
                </div>
         
        <?php endwhile;
        wp_reset_postdata();
    endif;
    $output = ob_get_clean();
    return $output;
}


/*CPT Team*/
function team_custom_post_type() {

    $labels = array(
        'name'                => _x( 'Team Member', 'linq-analytics' ),
        'singular_name'       => _x( 'Team Member', 'linq-analytics' ),
        'menu_name'           => __( 'Team', 'linq-analytics' ),
        'parent_item_colon'   => __( 'Parent Team Member', 'linq-analytics' ),
        'all_items'           => __( 'All Team Member', 'linq-analytics' ),
        'view_item'           => __( 'View Team Members', 'linq-analytics' ),
        'add_new_item'        => __( 'Add New Team Member', 'linq-analytics' ),
        'add_new'             => __( 'Add New', 'linq-analytics' ),
        'edit_item'           => __( 'Edit Team Members', 'linq-analytics' ),
        'update_item'         => __( 'Update Team Members', 'linq-analytics' ),
        'search_items'        => __( 'Search Team Members', 'linq-analytics' ),
        'not_found'           => __( 'Not Found', 'linq-analytics' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'linq-analytics' ),
    );
     
    $args = array(
        'label'               => __( 'Team', 'linq-analytics' ),
        'description'         => __( 'Team members information', 'linq-analytics' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'menu_icon'           => 'dashicons-businessman',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'show_in_rest' => true,
    );

    register_post_type( 'team', $args );
 
}
 
add_action( 'init', 'team_custom_post_type', 0 );

/**animation shortcode****/
add_shortcode('stay_connected_animation', 'stay_connected_animation');
function stay_connected_animation(){
    ob_start();
        
        get_template_part( 'shortcode/stay-connected', 'animation' );
    
    $output = ob_get_clean();
    return $output;
}

/**animation shortcode****/
add_shortcode('readyout_animation', 'readyout_animation');
function readyout_animation(){
    ob_start();
        
    get_template_part( 'shortcode/ready-out-of', 'animation' );

    $output = ob_get_clean();
    return $output;
}

/**animation shortcode****/
add_shortcode('mobile_readyout_animation', 'mobile_readyout_animation');
function mobile_readyout_animation(){
    ob_start();
        
    get_template_part( 'shortcode/mobile-ready-out-of', 'animation' );

    $output = ob_get_clean();
    return $output;
}

/**animation shortcode****/
add_shortcode('hierachy_animation', 'hierachy_animation');
function hierachy_animation(){
    ob_start();
        
        get_template_part( 'shortcode/hierarchy', 'animation' );

    $output = ob_get_clean();
    return $output;
}

/***Add login logo in the login screen*****/
function linqanalytics_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo home_url(); ?>/wp-content/uploads/logo.svg);
            height:65px;
            width:320px;
            background-size: 320px 65px;
            background-repeat: no-repeat;
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'linqanalytics_login_logo' );

function linqanalytics_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'linqanalytics_login_logo_url' );