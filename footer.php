<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package linqanalytics
 */

	$display_footer_cta = get_field('display_footer_cta');
	$heading = get_field('cta_heading', 'option');
	$content = get_field('cta_content', 'option');
	$cta_link = get_field('cta_link', 'option');
	$left_icon_one = get_field('left_icon_one', 'option');
	$left_icon_two = get_field('left_icon_two', 'option');
	$left_icon_three = get_field('left_icon_three', 'option');
	$right_icon_one = get_field('right_icon_one', 'option');
	$right_icon_two = get_field('right_icon_two', 'option');
	$right_icon_three = get_field('right_icon_three', 'option');

	$la_heading = get_field('cta_heading');
	$la_content = get_field('cta_content');
	$la_cta_link = get_field('cta_link');
	$la_left_icon_one = get_field('left_icon_one');
	$la_left_icon_two = get_field('left_icon_two');
	$la_left_icon_three = get_field('left_icon_three');
	$la_right_icon_one = get_field('right_icon_one');
	$la_right_icon_two = get_field('right_icon_two');
	$la_right_icon_three = get_field('right_icon_three');

	if($display_footer_cta == 'Yes'):
?>
	<!--cta section start-->
	<div class="cta-section">
		<div class="container center-align">
			<?php if(!empty($left_icon_one)): ?>
				<div class="icon-box icon-1"><img src="<?php echo $left_icon_one; ?>"  data-aos-delay="200" data-aos="fade-up"></div>
			<?php endif; ?>
			<?php if(!empty($left_icon_two)): ?>
		    	<div class="icon-box icon-2"><img src="<?php echo $left_icon_two; ?>"  data-aos-delay="400" data-aos="fade-left"></div>
		    <?php endif; ?>
		    <?php if(!empty($left_icon_three)): ?>
		    	<div class="icon-box icon-3"><img src="<?php echo $left_icon_three; ?>" data-aos-delay="600" data-aos="fade-left"></div>
		    <?php endif; ?>
		    <?php if(!empty($right_icon_one)): ?>
		    	<div class="icon-box icon-4"><img src="<?php echo $right_icon_one; ?>" data-aos-delay="200" data-aos="fade-right"></div>
		    <?php endif; ?>
		    <?php if(!empty($right_icon_two)): ?>
		    	<div class="icon-box icon-5"><img src="<?php echo $right_icon_two; ?>" data-aos-delay="400" data-aos="fade-right"></div>
		    <?php endif; ?>
		    <?php if(!empty($right_icon_three)): ?>
		    	<div class="icon-box icon-6"><img src="<?php echo $right_icon_three; ?>" data-aos-delay="600" data-aos="fade-right"></div>
		    <?php endif; ?>
		    <?php if(!empty($heading)): ?>
				<div class="content">
					<div >
					<?php if(!empty($heading)): ?>
						<h2  data-aos-delay="" data-aos="fade-up"><?php echo $heading; ?></h2>
					<?php endif; ?>
					<?php if(!empty($content)): ?>	
						<div class="subheading-b mb-40"  data-aos-delay="300" data-aos="fade-up"><?php echo $content; ?></div>
					<?php endif; ?>
					<?php if(!empty($cta_link)): ?>
					<div class="btn-row"  data-aos-delay="500" data-aos="fade-up"><a class="site-btn site-btn-pink" href="<?php echo $cta_link['url']; ?>" target="<?php echo $cta_link['target']; ?>"><?php echo $cta_link['title']; ?></a></div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div></div>
	  </div>
	<!--cta section-end-->
	<?php elseif($display_footer_cta == 'No' && !empty($la_heading)): ?>
		<!--cta section start-->
		<div class="cta-section">
			<div class="container center-align">
				<?php if(!empty($la_left_icon_one)): ?>
					<div class="icon-box icon-1"><img src="<?php echo $la_left_icon_one; ?>"  data-aos-delay="200" data-aos="fade-up"></div>
				<?php endif; ?>
				<?php if(!empty($la_left_icon_two)): ?>
			    	<div class="icon-box icon-2"><img src="<?php echo $la_left_icon_two; ?>"  data-aos-delay="400" data-aos="fade-left"></div>
			    <?php endif; ?>
			    <?php if(!empty($la_left_icon_three)): ?>
			    	<div class="icon-box icon-3"><img src="<?php echo $la_left_icon_three; ?>" data-aos-delay="600" data-aos="fade-left"></div>
			    <?php endif; ?>
			    <?php if(!empty($la_right_icon_one)): ?>
			    	<div class="icon-box icon-4"><img src="<?php echo $la_right_icon_one; ?>" data-aos-delay="200" data-aos="fade-right"></div>
			    <?php endif; ?>
			    <?php if(!empty($la_right_icon_two)): ?>
			    	<div class="icon-box icon-5"><img src="<?php echo $la_right_icon_two; ?>" data-aos-delay="400" data-aos="fade-right"></div>
			    <?php endif; ?>
			    <?php if(!empty($la_right_icon_three)): ?>
			    	<div class="icon-box icon-6"><img src="<?php echo $la_right_icon_three; ?>" data-aos-delay="600" data-aos="fade-right"></div>
			    <?php endif; ?>
			    <?php if(!empty($la_heading)): ?>
					<div class="content">
						<div >
						<?php if(!empty($la_heading)): ?>
							<h2  data-aos-delay="" data-aos="fade-up"><?php echo $la_heading; ?></h2>
						<?php endif; ?>
						<?php if(!empty($la_content)): ?>	
							<div class="subheading-b mb-40"  data-aos-delay="300" data-aos="fade-up"><?php echo $la_content; ?></div>
						<?php endif; ?>
						<?php if(!empty($la_cta_link)): ?>
						<div class="btn-row"  data-aos-delay="500" data-aos="fade-up"><a class="site-btn site-btn-pink" href="<?php echo $la_cta_link['url']; ?>" target="<?php echo $la_cta_link['target']; ?>"><?php echo $la_cta_link['title']; ?></a></div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div></div>
		  </div>
		<!--cta section-end-->
	<?php endif; ?>

	<footer id="colophon" class="site-footer">

		<div class="site-info container col">
			<div class="row">
				<div class="col l3 m12 s12 footer-logo">
					<?php
						$footer_logo = get_field('footer_logo', 'option');
					?>
					<a href="/"><img src="<?php echo $footer_logo; ?>" alt="Link analytics"></a>
				</div>
				<div class="col l2 m8 s4 footer-menu footer-menu1">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							)
						);
					?>
				</div>
				<div class="col l3 m4 s8 footer-menu footer-menu2">
					<?php
						wp_nav_menu(
							array(
								'menu_id'        => 'footer-menu',
							)
						);
					?>
				</div>		
		
			</div>
		</div><!-- .site-info -->

		<div class="footer-credit">
			<div class="container">
				 <div class="">
				 	<div class=" copyrighttxt">
			           <?php echo get_field('footer_credit_section','option'); ?>
			        </div>
                </div>
            </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
