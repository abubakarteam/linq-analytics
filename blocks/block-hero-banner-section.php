<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */

// create id attribute for specific styling
$id = 'hero-banner-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$heading = $block_fields['heading_left'];
$subheading = $block_fields['subheading_left'];
$content = $block_fields['banner_content_left'];
$cta_link = $block_fields['cta_link'];
$banner_image_right = $block_fields['banner_right_image'];
?>
<!-- banner section start-->
<section>
	<div class="banner-section">
	
		<div class="container col">
			<div class="row">
				<div class="col l8 s12">
					<div class="col-left">
						<h1 data-aos="fade-right" data-aos-delay="100"><?php echo $heading; ?></h1>
						<div class="subheading-h mb-20" data-aos="fade-right" data-aos-delay="300"><?php echo $subheading; ?></div>
						<div class="subheading-b mb-40" data-aos="fade-right" data-aos-delay="500">
							<?php echo $content; ?> 
						</div>
						<div class="btn-row" data-aos="fade-up" data-aos-delay="700">
							<a class="site-btn site-btn-pink" href="<?php echo $cta_link['url']; ?>" target="<?php echo $cta_link['target']; ?>"><?php echo $cta_link['title']; ?></a>
						</div>
					</div>
				</div>
				<div class="col l4 s12">
		        	<div class="banner-animation right-align" data-aos="fade-left">
		        		<?php 
			        		if(!empty($block_fields['animation'])){ 
			        			echo do_shortcode($block_fields['animation']); 
			        		}else{ ?>
								<img src="<?php echo $banner_image_right; ?>" align="Hero image">
						<?php } ?>	
					</div>
				 </div> 
				
			</div>      		
		</div>
	</div>
	
</section>
<!-- banner section end-->