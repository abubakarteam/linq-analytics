<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'image-content-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$image = $block_fields['image'];
$content = $block_fields['content'];

$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}

if(!empty($block_fields)):
?>
<!--image content section start-->
<section class="image-content-section mb-146 <?php echo $dynamic_class; ?>" data-aos="">
	<div class="container-xsm">
		<?php if(!empty($image)): ?>
			<img src="<?php echo $image; ?>" class="mb-15 rounded-10">
		<?php endif; ?>
		<?php if(!empty($image)): ?>
			<div class="txt-m"><?php echo $content; ?></div>
		<?php endif; ?>
	</div>
</section>
<!--image content section-end-->
<?php
endif;