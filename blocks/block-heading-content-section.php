<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'heading-content-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$heading = $block_fields['heading'];
$content = $block_fields['content'];
$paragraph_font = $block_fields['paragraph_font'];
$alignment = $block_fields['alignment'];
$bottom_space = $block_fields['bottom_space'];
$section_background_image = $block_fields['section_background_image'];

$dynamic_class = '';

if($paragraph_font == 'Small'){
	$dynamic_class = 'only-title-para-info-small';
}elseif($paragraph_font == 'Large'){
	$dynamic_class = 'only-title-para-info';
}else{
	$dynamic_class = 'only-title-para-info';
}

if($alignment == 'Left'){
	$dynamic_class .= ' left-align';
}elseif($alignment == 'Center'){
	$dynamic_class .= ' center-align';
}else{
	$dynamic_class .= '';
}

if($bottom_space == 'Space Bottom 50'){
	$dynamic_class .= ' mb-50';
}elseif($bottom_space == 'Space Bottom 100'){
	$dynamic_class .= ' mb-100';
}else{
	$dynamic_class .= '';
}

if($section_background_image == "Left"){
	$dynamic_class .= ' left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class .= ' right-union-bg';
}else{
	$dynamic_class .= '';
}
?>
<!--heading content section start-->
<section class="<?php echo $dynamic_class; ?> " data-aos="fade-up">
	<div class="container-sm">
		<h3><?php echo $heading; ?></h3>
		<div class="subheading-b"><?php echo $content; ?></div>
	</div>
</section>
<!--heading content section-end-->