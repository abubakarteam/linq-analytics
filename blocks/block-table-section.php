<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'table-section-' . $block['id'];
// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$heading_one = $block_fields['heading_one'];
$heading_two = $block_fields['heading_two'];
$heading_three = $block_fields['heading_three'];
$table_data = $block_fields['table_data'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
  $dynamic_class = 'left-union-bg'; 
}
elseif($section_background_image == "Right"){
  $dynamic_class = 'right-union-bg';
}else{
  $dynamic_class = '';
}
?>
<div class="data-tble-section mb-100 <?php echo $dynamic_class; ?>">
   <div class="container-xsm">

        <div  class="tbldesign">
       <table>
          <tr>
              <th><?php echo $heading_one; ?></th>
              <th><?php echo $heading_two; ?></th>
              <th><?php echo $heading_three; ?></th>
          </tr>
          <?php foreach ($table_data as $data) { ?>
            <tr>
                <td><?php echo $data['vive']; ?></td>
                <td><?php echo $data['mattis']; ?></td>
                <td><?php echo $data['mattis_copy']; ?></td>
            </tr>
        <?php } ?>
       </table>
      </div> </div>

</div>