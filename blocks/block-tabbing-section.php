<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'tabbing-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
  $dynamic_class = 'left-union-bg'; 
}
elseif($section_background_image == "Right"){
  $dynamic_class = 'right-union-bg';
}else{
  $dynamic_class = '';
}
?>
<section class="tabbing-section mb-50 <?php echo $dynamic_class; ?>" >
  <div class="container" >

  <div class = "col s12">
    <ul class = "tabs" data-aos-delay="" data-aos="fade-in">
      <?php 
          foreach ($block_fields as $block_field) {
            foreach ($block_field as $key => $value) {
              ?>
                <li class = "tab col s3"><a href = "#<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a></li>
              <?php
            }
          }
      ?>
    </ul>
  </div>
  <div class="tab-content col">

   <?php 
        foreach ($block_fields as $block_field) {
          foreach ($block_field as $key => $value) {
            ?>
              <div id = "<?php echo $value['title']; ?>" class ="row content">
                <div class="col m3 s12 center-align" >
                  <img src="<?php echo $value['image']; ?>" data-aos-delay="" data-aos="fade-right"></div>
                      <div class="col m9 s12">
                  <div class="subheading-m" data-aos-delay="" data-aos="fade-up"><?php echo $value['content']; ?></div></div>
              </div>
            <?php
          }
        }
    ?>   
  </div> 
</div></div>