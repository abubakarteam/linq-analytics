<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'faq-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
  $dynamic_class = 'left-union-bg'; 
}
elseif($section_background_image == "Right"){
  $dynamic_class = 'right-union-bg';
}else{
  $dynamic_class = '';
}
?>
<section class="faq-section mb-160 <?php echo $dynamic_class; ?>" >
  <div class="container-xsm" >
    <div class="faq-content">
      <?php if(!empty($block_fields['faq_section_title'])): ?>
        <h3 class="faq-heading mb-60 center-align"><?php echo $block_fields['faq_section_title']; ?></h3>
      <?php endif; ?>
      <ul class = "collapsible" data-aos-delay="" data-aos="">
     <?php 
          foreach ($block_fields['faq'] as $block_field){
            ?>
              <li>
                 <div class="collapsible-header"><?php echo $block_field['faq_title'];  ?> <div class="icon"><span></span><span></span></div></div>
                 <div class="collapsible-body"><span><?php echo $block_field['faq_content']; ?></span></div>
              </li>
            <?php
          }
      ?> 
      </ul>  
    </div> 
  </div>
</section>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('.collapsible').collapsible();
  });
</script>