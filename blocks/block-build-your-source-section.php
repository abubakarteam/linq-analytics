<?php 
/**
 * Block Name: Build Your Source Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'build-your-source-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$graphic_alignment = $block_fields['graphic_alignment'];
$left_image =  $block_fields['left_image'];
$center_icon = $block_fields['center_icon'];
$right_image = $block_fields['right_image'];
$heading = $block_fields['heading'];
$content = $block_fields['content'];
$cta_button_link = $block_fields['cta_button_link'];
$alignment = ($graphic_alignment == 'Left') ? 'left-image' : 'right-image' ;
$image_type = $block_fields['image_type'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}
?>
<!-- build your source section start-->
<section>
	<div class="two-column-section mb-160 <?php echo $dynamic_class; ?>">
		<div class="container col">
			<div class="row d-flex <?php echo $alignment; ?>">
				<div class="col l6 s12 mb-20">
					<?php if($image_type == 'Build Your Source'){ ?>
						<!---sot image section--->
						<div class=" sot-img-col">
							<div class="row ">
								<div class="col l6 s6"><img class="left-img" data-aos-delay="" data-aos="fade-up" src="<?php echo $left_image; ?>"></div>
								<div class="col l6 s6"><img class="right-img" data-aos-delay="700" data-aos="fade-up" src="<?php echo $right_image; ?>"></div>
								<div class="center-box">
									<div class="circle-img" data-aos="zoom-in" data-aos-delay="1000"> <img src="<?php echo $center_icon; ?>"></div>
								</div>
							</div>
						</div>
					<?php }elseif ($image_type == 'Cloud Campus') { ?>
						<!---clouyd image col---->
						<div class="cloud-img-col ">
							<div class="img1">
							<div data-aos-delay="" data-aos="fade-right"> <img src="<?php echo $left_image; ?>"> </div> </div>
							<div  class="img2">
					
							<div data-aos-delay="300" data-aos="fade-up"> <img src="<?php echo $right_image; ?>"> <div class="lines-block"></div></div> </div>
							<div class="img3">
							<div data-aos-delay="600" data-aos="zoom-in" class="circle-img"> <img src="<?php echo $center_icon; ?>">  </div>
							</div>
						</div>
					<?php }elseif ($image_type == 'Single Image') { ?>
							<!---clouyd image col---->
							<div class="single-img-col ">
								<div class="img1">
									<div data-aos-delay="" data-aos="fade-right"> <img src="<?php echo $left_image; ?>" class="rounded-10"> </div>
								</div>
							</div>

					<?php	}else{ ?>
						<!--No Merge needed img col-->
						<div class="no-merge-img-col"> <img src="<?php echo $left_image; ?>" class="img1">

							<div class="tick1 tick"><div data-aos-delay="200" data-aos="fade-left"><img src="/wp-content/uploads/tick-icon.svg"></div></div>

								<div class="tick2 tick"><div data-aos-delay="500" data-aos="fade-left"><img src="/wp-content/uploads/tick-icon.svg"></div></div>

							<div class="tick3 tick"><div data-aos-delay="800" data-aos="fade-left"><img src="/wp-content/uploads/tick-icon.svg"></div></div>

						</div>
					<?php } ?>
				</div>
				<div class="col l6 s12 mb-20">
					<div class="content">
						<h2><?php echo $heading; ?></h2>
						<div class="txt-b  mb-40">
							<?php echo $content; ?>
						</div>
						<div class="btn-row">
							<a class="site-btn site-btn-pink" href="<?php echo $cta_button_link['url']; ?>" target="<?php echo $cta_button_link['target']; ?>"><?php echo $cta_button_link['title']; ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- build your source section end-->