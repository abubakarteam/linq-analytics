<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'video-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$play_icon = $block_fields['video_play_icon'];
$background_image = $block_fields['video_background_image'];
$video_link = $block_fields['video_link'];
$text_on_video = $block_fields['text_on_video'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}
?>
<!-- video section start-->
<section>
	<div class="video-section mb-160 <?php echo $dynamic_class; ?>">
		<div class="container">
			<div class="align-center" data-aos-delay="" data-aos="fade-up">
				<div class="videobox" style="background-image:url(<?php echo $background_image ?>)">
					<a class="waves-effect waves-light play-icon center-box modal-trigger" href="#modal1"></a>     
					<div class="txt-big-bold"><?php echo $text_on_video; ?></div>                        
				</div>
			</div>
		</div>
	</div>	
</section>
<!-- Modal Structure -->
<div id="modal1" class="modal video-popup">
	<a href="javascript:void(0)" class="modal-close waves-effect waves-green btn-flat">x</a>	
	<div class="modal-content">
        <div class="iframe-div">
			<iframe class="responsive-iframe" src="<?php echo $video_link; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
	</div>
</div>
<!-- video section end-->