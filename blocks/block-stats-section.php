<?php 
/**
 * Block Name: Stats Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */

// create id attribute for specific styling
$id = 'stats-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$left_heading = $block_fields['left_heading'];
$left_content = $block_fields['left_content'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}
?>
<!-- stats section start-->
<section class="stats-section mb-136 <?php echo $dynamic_class; ?>">

		<div class="container col">
			<div class="row">
				<div class="col m5 s12">
					<div class="col-left"  data-aos-delay="" data-aos="fade-left">
						<h2 data-aos-delay="" data-aos="fade-right"><?php echo $left_heading; ?></h2>
						<div class="txt-b mb-40" data-aos-delay="300" data-aos="fade-right">
							<?php echo $left_content; ?> 
						</div>
					</div>
				</div>
				<div class="col m7 s12">
				<div class="col-right"  data-aos-delay="" data-aos="fade-left">
					<?php 
						foreach ($block_fields['stats'] as $key => $stat) {
							$no_stats = (empty($stat['stat_number'])) ? 'no-stats' : '' ;
							?>
							<div class="stats <?php echo $no_stats; ?>">
								<?php if(!empty($stat['stat_number'])): ?>
										<div class="stat-number tbl-col"><?php echo $stat['stat_number']; ?></div>
								<?php endif; ?>
								<?php if(!empty($stat['stat_info'])): ?>	
										<div class="stat-info txt-big-bold tbl-col"><?php echo $stat['stat_info']; ?></div>
								<?php endif; ?>	
							</div>
							<?php
						}
					?>
				</div> 
				</div> 
			</div>      		
		</div>

</section>
<!-- stats section end-->