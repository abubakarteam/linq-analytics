<?php 
/**
 * Block Name: Team Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'team-section-' . $block['id'];
// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$team_section_heading = $block_fields['team_section_heading'];
$team_members = $block_fields['select_team_members'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
  $dynamic_class = 'left-union-bg'; 
}
elseif($section_background_image == "Right"){
  $dynamic_class = 'right-union-bg';
}else{
  $dynamic_class = '';
}

if(!empty($block_fields)):
?>
<!--team section-->
<div class="team-section mb-136 <?php echo $dynamic_class; ?>">
   <div class="container">
        <?php if(!empty($team_section_heading)): ?>
          <h3 class="team-heading center-align mb-60"><?php echo $team_section_heading; ?></h3>
        <?php endif; ?>
        <?php if(!empty($team_members)): ?>
          <div class="team-members col">
            <div class="row">
              <?php 
                  foreach ( $team_members as $team_member) {
                    ?>
                    <div class="col m4 s12 center-align">
                      <div class="member-info">
                        <img src="<?php echo get_the_post_thumbnail_url($team_member); ?>" class="rounded-100 mb-20">
                        <h6 class="member-name"><?php echo get_the_title($team_member); ?></h6>
                        <div class="member-des txt-medium-bold mb-20"><?php echo get_field('member_designation', $team_member); ?></div>
                        <div class="member-bio txt-m"><?php echo get_field('member_bio', $team_member); ?></div>
                      </div>  </div> 
                    <?php
                  }
              ?>
          </div></div>
        <?php endif; ?>
   </div>
</div>
<!--team section end-->
<?php 
endif;