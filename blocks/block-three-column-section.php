<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'three-column-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$column_one_icon = $block_fields['column_one_icon'];
$column_one_heading = $block_fields['column_one_heading'];
$column_one_content = $block_fields['column_one_content'];

$column_two_image = $block_fields['column_two_image'];
$column_two_heading = $block_fields['column_two_heading'];
$column_two_content = $block_fields['column_two_content'];

$column_three_image = $block_fields['column_three_image'];
$column_three_heading = $block_fields['column_three_heading'];
$column_three_content = $block_fields['column_three_content'];

$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
  $dynamic_class = 'left-union-bg'; 
}
elseif($section_background_image == "Right"){
  $dynamic_class = 'right-union-bg';
}else{
  $dynamic_class = '';
}
?>
<!-- 3 column section -->
<div class="three-column-boxes mb-160 <?php echo $dynamic_class; ?>">
<div class="container col">
  <div class="row">
     <div class="col m4 s12 mb-40 three-column" data-aos-delay="" data-aos="fade-up">
       <div class="column-box center-align">
            <figure>
            <img src="<?php echo $column_one_icon; ?>"></figure>
            <h5><?php echo $column_one_heading; ?></h5>
            <div class="txt-m"><?php echo $column_one_content; ?></div>
       </div>
     </div>
     <div class="col m4 s12 mb-40 three-column" data-aos-delay="" data-aos="fade-up">
       <div class="column-box center-align"><figure>
            <img src="<?php echo $column_two_image; ?>"></figure>
            <h5><?php echo $column_two_heading; ?></h5>
            <div class="txt-m"><?php echo $column_two_content; ?></div>
       </div>
     </div>
     <div class="col m4 s12 mb-40 three-column" data-aos-delay="" data-aos="fade-up">
       <div class="column-box center-align"><figure>
            <img src="<?php echo $column_three_image; ?>"></figure>
            <h5><?php echo $column_three_heading; ?></h5>
            <div class="txt-m"><?php echo $column_three_content; ?></div>
       </div>
     </div>
  </div>
</div>
</div>
<!-- 3 column section end-->