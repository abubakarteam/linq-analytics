<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */

// create id attribute for specific styling
$id = 'logo-with-info-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$logo_one = $block_fields['logo_one'];
$logo_two = $block_fields['logo_two'];
$logo_three = $block_fields['logo_three'];

$heading = $block_fields['heading'];
$subheading = $block_fields['subheading'];
$logo_one_title = $block_fields['logo_one_title'];
$logo_one_description = $block_fields['logo_one_description'];
$logo_two_title = $block_fields['logo_two_title'];
$logo_two_description = $block_fields['logo_two_description'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}
?>
<!-- logo with info section start-->

<section  class="logo-info-section mb-200 <?php echo $dynamic_class; ?>">
	<div class="container col">
		<div class="sky-blue-bg">
	<div class="row">
		<div class="col m4 s12">
			<div class="col-left  data-aos-delay="" data-aos="fade-left"">
		
				<div class="hide-on-small-only">
			  <?php 
	        		if(!empty($block_fields['animation'])){ 
	        			echo do_shortcode($block_fields['animation']); 
	        		}else{ ?>
						<img src="<?php echo $logo_one; ?>">
			  <?php } 
                  ?></div><div class="hide-on-med-and-up"><?php
	        		if(!empty($block_fields['mobile_animation'])){ 
	        			echo do_shortcode($block_fields['mobile_animation']); 
	        		}else{ ?>
						<img src="<?php echo $logo_two; ?>">
			  <?php } ?>	  </div>

			</div>
		</div>
		<div class="col m8 s12">
			<div class="col-right"  data-aos-delay="" data-aos="fade-right">
			<h2><?php echo $heading; ?></h2>
			<div class="txt-b mb-50"><?php echo $subheading; ?></div>
			<h5><?php echo $logo_one_title; ?></h5>
			<div class="txt-m mb-50"><?php echo $logo_one_description; ?></div>
			<h5><?php echo $logo_two_title; ?></h5>
			<div class="txt-m"><?php echo $logo_two_description; ?></div>
		</div></div>
	</div>
		</div>
</div>
</section>
<!-- logo with info section end-->