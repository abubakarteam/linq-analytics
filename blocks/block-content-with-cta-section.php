<?php 
/**
 * Block Name: Content with CTA Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'content-with-cta-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$heading = $block_fields['heading'];
$content = $block_fields['content'];
$button_cta_link = $block_fields['button_cta_link'];
$left_icon_one = $block_fields['left_icon_one'];
$left_icon_two = $block_fields['left_icon_two'];
$left_icon_three = $block_fields['left_icon_three'];
$right_icon_one = $block_fields['right_icon_one'];
$right_icon_two = $block_fields['right_icon_two'];
$right_icon_three = $block_fields['right_icon_three'];
$cta_type = $block_fields['cta_type'];
$dynamic_class = ($cta_type == 'CTA Section Round') ? 'cta-section-rounded mb-146' : 'cta-section' ;
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$bg_class = ' left-union-bg';	
}
elseif($section_background_image == "Right"){
	$bg_class = ' right-union-bg';
}else{
	$bg_class = '';
}
?>
<!--cta section start-->
<div class="<?php echo $dynamic_class.' '.$bg_class; ?>">
	<div class="container center-align">
		<?php if(!empty($left_icon_one)): ?>
			<div class="icon-box icon-1"><img src="<?php echo $left_icon_one; ?>"  data-aos-delay="200" data-aos="fade-up"></div>
		<?php endif; ?>
		<?php if(!empty($left_icon_two)): ?>
	    	<div class="icon-box icon-2"><img src="<?php echo $left_icon_two; ?>"  data-aos-delay="400" data-aos="fade-left"></div>
	    <?php endif; ?>
	    <?php if(!empty($left_icon_three)): ?>
	    	<div class="icon-box icon-3"><img src="<?php echo $left_icon_three; ?>" data-aos-delay="600" data-aos="fade-left"></div>
	    <?php endif; ?>
	    <?php if(!empty($right_icon_one)): ?>
	    	<div class="icon-box icon-4"><img src="<?php echo $right_icon_one; ?>" data-aos-delay="200" data-aos="fade-right"></div>
	    <?php endif; ?>
	    <?php if(!empty($right_icon_two)): ?>
	    	<div class="icon-box icon-5"><img src="<?php echo $right_icon_two; ?>" data-aos-delay="400" data-aos="fade-right"></div>
	    <?php endif; ?>
	    <?php if(!empty($right_icon_three)): ?>
	    	<div class="icon-box icon-6"><img src="<?php echo $right_icon_three; ?>" data-aos-delay="600" data-aos="fade-right"></div>
	    <?php endif; ?>
	    <?php if(!empty($heading)): ?>
			<div class="content">
				<div >
				<?php if(!empty($heading)): ?>
					<h2  data-aos-delay="" ><?php echo $heading; ?></h2>
				<?php endif; ?>
				<?php if(!empty($content)): ?>	
					<div class="subheading-b mb-40" ><?php echo $content; ?></div>
				<?php endif; ?>
				<?php if(!empty($button_cta_link)): ?>
				<div class="btn-row"><a class="site-btn site-btn-pink" href="<?php echo $button_cta_link['url']; ?>" target="<?php echo $button_cta_link['target']; ?>"><?php echo $button_cta_link['title']; ?></a></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div></div>
  </div>

<!--cta section-end-->

