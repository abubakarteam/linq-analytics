<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'testimonial-section-' . $block['id'];
// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$quote_icon = $block_fields['quote_icon'];
$shortcode = $block_fields['testimonial_shortcode'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}
?>
<!--quote section-->



<div class="quote-section mb-200 <?php echo $dynamic_class; ?>">
   <div class="container">
        <div class="quote-box" data-aos-delay="" data-aos="fade-left">
        	<div class="quote-icon"><img src="<?php echo $quote_icon; ?>"></div>
            <?php if($shortcode != ''):?>
				<?php echo do_shortcode($shortcode);?>
			<?php endif;?>
        </div>
   </div>
   </div>





   

<!--quote section end-->