<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */

// create id attribute for specific styling
$id = 'logo-list-section-' . $block['id'];
// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$logo_display_type = $block_fields['logo_display_type'];
$logo_section_heading = $block_fields['logo_section_heading'];
$logos = $block_fields['logos'];
$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}

if($logo_display_type == 'Logo List'):
?>
<!-- logo list section start-->
<section>
	<div class="logo-list mb-100 <?php echo $dynamic_class; ?>">
	   <div class="container center-align">
	   		<div class="txt-small-bold mb-40"><?php echo $logo_section_heading; ?></div>
	
	        	<?php foreach ($logos as $logo) {
	        		?>
	        			<div class="logobox"><img src="<?php echo $logo['logo']; ?>"></div>
	        		<?php		
	        	} ?>
	
	   	</div>
	</div>
</section>
<!-- logo list section end-->
<?php
elseif($logo_display_type == 'Logo Grid'):
?>
<!-- logo gird section start-->
<section>
	<div class="logo-grid mb-100 <?php echo $dynamic_class; ?>">
	   <div class="container-xsm center-align">
	   		<h4 class="mb-40"><?php echo $logo_section_heading; ?></h4>
</div>
	   		   <div class="container center-align">
	             <div class="logo-setion">
	   			<?php foreach ($logos as $logo) { ?>
		   			
				        <div class="logobox">
				            <img src="<?php echo $logo['logo']; ?>">
				        </div>
		        
	            <?php } ?>
	     	</div>
	   	</div>
	</div>
</section>
<!-- logo grid section end-->
<?php
endif;