<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */
// create id attribute for specific styling
$id = 'two-column-with-form' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$heading = $block_fields['heading'];
$subheading = $block_fields['subheading'];

$icon_one = $block_fields['icon_one'];
$heading_one = $block_fields['heading_one'];
$content_one = $block_fields['content_one'];

$icon_two = $block_fields['icon_two'];
$heading_two = $block_fields['heading_two'];
$content_two = $block_fields['content_two'];

$icon_three = $block_fields['icon_three'];
$heading_three = $block_fields['heading_three'];
$content_three = $block_fields['content_three'];

$form = $block_fields['gravity_form_shortcode'];
$policy_text = $block_fields['policy_text'];

$section_background_image = $block_fields['section_background_image'];

if($section_background_image == "Left"){
  $dynamic_class = 'left-union-bg'; 
}
elseif($section_background_image == "Right"){
  $dynamic_class = 'right-union-bg';
}else{
  $dynamic_class = '';
}
?>
<div class="demo-page-top-section <?php echo $dynamic_class; ?>">
   <div class="container col">
        <div class="row">
           <div class="col-left col l6 s12">
            <div class="pr-80">
                  <div class="page-top-section mb-80 mt-40" data-aos-delay="" data-aos="fade-left">
                       <h1><?php echo $heading; ?></h1>
                       <div class="subheading-b "><?php echo $subheading; ?></div>
                  </div>
          
                  <div class="three-listing-view col hide-on-med-and-down">
                    <div class="row">
                       <div class="col s12 mb-60" data-aos-delay="" data-aos="fade-up">
                         <div class="column-box">
                              <img src="<?php echo $icon_one; ?>">
                              <h5><?php echo $heading_one; ?></h5>
                              <div class="txt-m"><?php echo $content_one; ?></div>
                         </div>
                       </div>
                        <div class="col s12 mb-60" data-aos-delay="300" data-aos="fade-up">
                         <div class="column-box">
                              <img src="<?php echo $icon_two; ?>">
                              <h5><?php echo $heading_two; ?></h5>
                              <div class="txt-m"><?php echo $content_two; ?></div>
                         </div>
                       </div>
                       <div class="col s12 mb-60" data-aos-delay="600" data-aos="fade-up">
                         <div class="column-box">
                              <img src="<?php echo $icon_three; ?>">
                              <h5><?php echo $heading_three; ?></h5>
                              <div class="txt-m"><?php echo $content_three; ?></div>
                         </div>
                       </div>
                  </div>
                </div></div>
            </div>
            <div class="col-left col l6 s12  mb-40" >
              <div data-aos-delay="" data-aos="fade-left">
              <?php echo do_shortcode($form);?>
              <div class="policytxt"><?php echo $policy_text; ?></div></div>
            </div>
              <div class="col s12 hide-on-large-only mb-40" >
              <div>
                <div class="three-listing-view col">
                    <div class="row">
                       <div class="col s12 mb-60" data-aos-delay="" data-aos="fade-up">
                         <div class="column-box">
                              <img src="<?php echo $icon_one; ?>">
                              <h5><?php echo $heading_one; ?></h5>
                              <div class="txt-m"><?php echo $content_one; ?></div>
                         </div>
                       </div>
                        <div class="col s12 mb-60" data-aos-delay="300" data-aos="fade-up">
                         <div class="column-box">
                              <img src="<?php echo $icon_two; ?>">
                              <h5><?php echo $heading_two; ?></h5>
                              <div class="txt-m"><?php echo $content_two; ?></div>
                         </div>
                       </div>
                       <div class="col s12 mb-60" data-aos-delay="600" data-aos="fade-up">
                         <div class="column-box">
                              <img src="<?php echo $icon_three; ?>">
                              <h5><?php echo $heading_three; ?></h5>
                              <div class="txt-m"><?php echo $content_three; ?></div>
                         </div>
                       </div>
                  </div>
                </div>
                    
              </div>
            </div>
        </div>
    </div>
</div>    
                  