<?php 
/**
 * Block Name: Client Connected Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */

// create id attribute for specific styling
$id = 'client-connected-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );

$section_background_image = $block_fields['section_background_image'];
$union_logo = $block_fields['small_union_logo'];
$heading_client_connected = $block_fields['heading_client_connected'];
$about_linq_analytics = $block_fields['about_linq_analytics'];

if($section_background_image == "Left"){
	$dynamic_class = 'left-union-bg';	
}
elseif($section_background_image == "Right"){
	$dynamic_class = 'right-union-bg';
}else{
	$dynamic_class = '';
}

?>
<!-- About section start-->
<section>
	<div class="about-section mb-136 <?php echo $dynamic_class; ?>">
		<div class="container-sm">
			<div class="center-align">
				<img src="<?php echo $union_logo; ?>" data-aos="fade-up">
				<div class="txt-s mb-40"  data-aos-delay="200" data-aos="fade-up"><?php echo $heading_client_connected;  ?></div>
		  		<div class="txt-h"  data-aos-delay="400" data-aos="fade-up"><?php echo $about_linq_analytics; ?></div>
		   </div>
		</div>   
	</div>
</section>
<!-- About section end-->