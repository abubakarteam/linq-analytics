<?php 
/**
 * Block Name: Hero Banner Block
* The template for displaying the custom gutenberg block
 *
 * @link https://www.advancedcustomfields.com/resources/blocks/
 *
 * @package Linq Analytics
 * @since 1.0.0
 *
 */

// create id attribute for specific styling
$id = 'toolkit-banner-section-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Get the class name for the block to be used for it.
$class_name = $block['className'];

// Meta fields related to current block
$block_fields = get_fields( $block['id'] );
$heading = $block_fields['heading'];
$subheading = $block_fields['subheading'];
$cta_link_one = $block_fields['cta_link_one'];
$cta_link_two = $block_fields['cta_link_two'];

if(!empty($block_fields)):
?>
<!-- banner section start-->
<section>
	<div class="toolkit-banner-section mb-50">
		<div class="container col">
			<div class="row">
				<div class="center-align">
					<h1 data-aos="" data-aos-delay="100"><?php echo $heading; ?></h1>
					<div class="subheading-b mb-50" data-aos="fade-right" data-aos-delay="300"><?php echo $subheading; ?></div>
					<div class="btn-row" data-aos="fade-up" data-aos-delay="700">
						<a class="site-btn site-btn-pink" href="<?php echo $cta_link_one['url']; ?>" target="<?php echo $cta_link_one['target']; ?>"><?php echo $cta_link_one['title']; ?></a>

						<a class="site-btn site-btn-white" href="<?php echo $cta_link_two['url']; ?>" target="<?php echo $cta_link_two['target']; ?>"><?php echo $cta_link_two['title']; ?></a>
					</div>
				
				</div>
			</div>      		
		</div>
	</div>
	
</section>
<!-- banner section end-->
<?php
endif;