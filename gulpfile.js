var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	header = require('gulp-header'),
	rename = require('gulp-rename'),
	cssmin = require('gulp-cssmin'),
	cssnano = require('gulp-cssnano'),
	concat = require('gulp-concat'),
	sourcemaps = require('gulp-sourcemaps'),
	package = require('./package.json'),
	fileinclude = require('gulp-file-include'),
	log = require('fancy-log');

var banner = [
	'/*!\n' +
	' * <%= package.name %>\n' +
	' * <%= package.title %>\n' +
	' * <%= package.url %>\n' +
	' * @author <%= package.author %>\n' +
	' * @version <%= package.version %>\n' +
	' * Copyright ' + new Date().getFullYear() + '. <%= package.license %> licensed.\n' +
	' */',
	'\n'
].join('');

var jsFiles = ['src/js/scripts.js'],
	jsDest = 'app/assets/js';

gulp.task('sass', function() {
    return gulp.src('src/scss/style.scss').pipe(sourcemaps.init()).pipe(sass().on('error', sass.logError)).pipe(cssmin()).pipe(sourcemaps.write('.')).pipe(gulp.dest('app/assets/css'));
});

gulp.task('js', function () {
	gulp.src(jsFiles)
		.pipe(sourcemaps.init())
		// .pipe(concat('scripts.js'))
		.pipe(fileinclude({
			prefix: '@@',
			basepath: 'src/'
		}))
		.pipe(header(banner, {
			package: package
		}))
		.pipe(gulp.dest(jsDest))
		.pipe(uglify())
		.pipe(header(banner, {
			package: package
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(sourcemaps.write(''))
		.pipe(gulp.dest(jsDest))
		.pipe(browserSync.reload({
			stream: true,
			once: true
		}));
});

gulp.task('html', function () {
	gulp.src(['src/pages/*.html'])
		.pipe(fileinclude({
			prefix: '@@',
			basepath: 'src/'
		}))
		.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('app/'))
		.pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', function () {
	browserSync.init(null, {
		server: {
			baseDir: "app"
		}
	});
});

gulp.task('bs-reload', function () {
	browserSync.reload();
});

gulp.task('css', function() {
    return gulp.src(['bower_components/materialize/dist/css/materialize.css', 'bower_components/aos/dist/aos.css', 'app/assets/css/style.css']).pipe(concat('bundle.css')).pipe(cssmin(gulp.dest('app/assets/css'))).pipe(gulp.dest('app/assets/css'));
});

gulp.task('default', ['sass', 'js', 'html', 'browser-sync', 'css'], function () {
	gulp.watch("src/scss/**/*.scss", ['sass']);
	gulp.watch("src/js/**/*.js", ['js']);
	gulp.watch("src/**/*.html", ['html']);
	gulp.watch("app/*.html", ['bs-reload']);
	gulp.watch("app/assets/css/style.css", ['css']);
});
