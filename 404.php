<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package linqanalytics
 */

get_header();
?>

	<main id="primary" class="site-main">
  <div class="container">
		<section class="error-404 not-found">
			<div class="page-header center-align">
				<h3 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'linqanalytics' ); ?></h3>
			</div><!-- .page-header -->

			<div class="page-content center-align">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'linqanalytics' ); ?></p>

					<?php
					get_search_form();

					?>

				

				
			</div><!-- .page-content -->
		</section><!-- .error-404 -->
 </div>
	</main><!-- #main -->

<?php
get_footer();
