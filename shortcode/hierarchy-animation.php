<svg width="638" height="507" viewBox="0 0 638 507" fill="none" xmlns="http://www.w3.org/2000/svg">
<path opacity="0.7" d="M313.782 183.452H280.422C269.376 183.452 260.422 192.406 260.422 203.452V254.926C260.422 265.972 251.468 274.926 240.422 274.926H167.515" stroke="#15377C" stroke-opacity="0.5" stroke-dasharray="5 5"/>
<path opacity="0.7" d="M313.782 425.14H280.422C269.377 425.14 260.422 416.185 260.422 405.14V353.666C260.422 342.62 251.468 333.666 240.422 333.666H163.26" stroke="#15377C" stroke-opacity="0.5" stroke-dasharray="5 5"/>
<path opacity="0.7" d="M313.137 304.121L160.895 304.121" stroke="#15377C" stroke-opacity="0.5" stroke-dasharray="5 5"/>
<path opacity="0.7" d="M315.126 61.9355H253.069C242.023 61.9355 233.069 70.8899 233.069 81.9355V224.883C233.069 235.929 224.115 244.883 213.069 244.883H156.64" stroke="#15377C" stroke-opacity="0.5" stroke-dasharray="5 5"/>
<g filter="url(#filter0_d)">
<rect x="20" y="206.257" width="161.424" height="161.424" rx="80.7121" fill="white"/>
</g>
<path fill-rule="evenodd" clip-rule="evenodd" d="M80.2068 275.94C82.6602 276.603 85.1419 276.379 87.3179 275.464L91.8162 282.207C92.3877 283.064 93.5393 283.291 94.3883 282.714C95.2373 282.138 95.4623 280.976 94.8908 280.119L90.4543 273.468C91.9996 272.072 93.1737 270.222 93.7506 268.049C95.3313 262.097 91.8304 255.978 85.9311 254.383C80.0318 252.788 73.968 256.321 72.3873 262.273C70.8066 268.226 74.3075 274.345 80.2068 275.94ZM101.855 315C110.156 315 116.885 308.21 116.885 299.835C116.885 291.459 110.156 284.669 101.855 284.669C93.5549 284.669 86.826 291.459 86.826 299.835C86.826 308.21 93.5549 315 101.855 315ZM139.429 264.291C139.429 269.787 135.223 274.243 130.035 274.243C128.254 274.243 126.589 273.718 125.17 272.806L115.654 284.255C114.996 285.046 113.827 285.15 113.043 284.486C112.259 283.822 112.156 282.643 112.814 281.852L122.477 270.226L122.487 270.215C121.328 268.559 120.642 266.51 120.642 264.291C120.642 258.794 124.848 254.338 130.035 254.338C135.223 254.338 139.429 258.794 139.429 264.291ZM69.9751 297.775C66.6465 300.61 61.6703 300.186 58.8605 296.827C56.0506 293.468 56.4711 288.447 59.7998 285.612C63.1284 282.776 68.1046 283.201 70.9144 286.56C72.2453 288.15 72.8515 290.114 72.7672 292.042L81.8464 293.72C82.8553 293.893 83.5337 294.859 83.3618 295.877C83.1898 296.895 82.2326 297.58 81.2237 297.406L71.7292 295.657C71.2839 296.44 70.6981 297.159 69.9751 297.775ZM142.076 310.464C145.303 308.034 145.959 303.42 143.542 300.158C141.124 296.896 136.548 296.221 133.32 298.652C132.545 299.235 131.919 299.945 131.447 300.731L122.948 298.865C121.897 298.635 120.861 299.307 120.635 300.367C120.409 301.427 121.078 302.474 122.13 302.705L130.396 304.519C130.391 306.064 130.866 307.624 131.854 308.958C134.272 312.22 138.848 312.895 142.076 310.464Z" fill="#FF3370"/>
<g filter="url(#filter1_dd)">
<rect x="306.158" y="25.167" width="248.414" height="75.3313" rx="10" fill="white"/>
</g>
<rect x="392" y="45" width="127" height="6" rx="3" fill="#C7D0E3"/>
<g opacity="0.8" clip-path="url(#clip0)">
<path d="M347.547 50.3325L339.645 43.3077C339.363 43.0572 338.953 43.0141 338.626 43.2006L326.333 50.2254C325.911 50.4659 325.765 51.0022 326.005 51.4233C326.06 51.5198 326.133 51.6049 326.22 51.6743L335.001 58.699C335.157 58.8237 335.35 58.8918 335.549 58.8922C335.711 58.8923 335.871 58.8473 336.009 58.7623L347.424 51.7375C347.838 51.4835 347.967 50.9427 347.713 50.5296C347.668 50.4561 347.612 50.3897 347.547 50.3325Z" fill="#004AFF"/>
<path d="M347.463 65.1944L336.047 57.2915C335.73 57.0714 335.305 57.085 335.002 57.3248L326.221 64.2794C325.841 64.5807 325.777 65.133 326.079 65.513C326.148 65.5998 326.232 65.6728 326.328 65.7282L338.622 72.8233C338.931 73.003 339.319 72.9765 339.602 72.7566L347.505 66.6098C347.888 66.3124 347.957 65.7608 347.66 65.3777C347.605 65.3071 347.539 65.2453 347.466 65.1943L347.463 65.1944Z" fill="#004AFF"/>
<path d="M367.708 64.3508L358.927 57.3261C358.623 57.0808 358.194 57.0658 357.874 57.2892L346.458 65.1921C346.06 65.4682 345.96 66.0152 346.236 66.4139C346.287 66.4874 346.349 66.5528 346.42 66.6076L354.323 72.7544C354.603 72.9731 354.988 73.0008 355.297 72.8246L367.591 65.7998C368.012 65.5593 368.158 65.023 367.918 64.6018C367.863 64.5054 367.79 64.4202 367.703 64.3509L367.708 64.3508Z" fill="#004AFF"/>
<path d="M367.595 50.2259L355.302 43.2011C354.974 43.0146 354.565 43.0577 354.283 43.3082L346.38 50.333C346.018 50.6548 345.984 51.2098 346.306 51.5726C346.363 51.637 346.43 51.6928 346.503 51.738L357.919 58.7627C358.057 58.8472 358.216 58.8916 358.379 58.8909C358.578 58.8911 358.771 58.8236 358.927 58.6995L367.708 51.6747C368.087 51.3722 368.149 50.8197 367.846 50.4407C367.777 50.3538 367.692 50.281 367.595 50.2259Z" fill="#004AFF"/>
<path d="M356.173 74.3459C355.247 74.8748 354.093 74.793 353.251 74.1387L346.963 69.2529L340.678 74.1422C339.832 74.7995 338.673 74.8793 337.745 74.3441L334.67 72.5703V75.5752C334.67 75.8803 334.828 76.1634 335.088 76.3233L346.503 83.3481C346.785 83.521 347.14 83.521 347.422 83.3481L358.837 76.3233C359.097 76.1639 359.256 75.8806 359.257 75.5752V72.5897L356.173 74.3459Z" fill="#004AFF"/>
</g>
<g filter="url(#filter2_dd)">
<rect x="306.158" y="145.338" width="296.841" height="75.3313" rx="10" fill="white"/>
</g>
<rect x="392" y="165" width="185" height="6" rx="3" fill="#C7D0E3"/>
<circle cx="400.772" cy="195.111" r="8.51961" fill="white"/>
<circle cx="400.772" cy="195.111" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="435.746" cy="195.111" r="8.51961" fill="white"/>
<circle cx="435.746" cy="195.111" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="470.722" cy="195.111" r="8.51961" fill="white"/>
<circle cx="470.722" cy="195.111" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="435.747" cy="195.111" r="3.1388" fill="#FF3370"/>
<g opacity="0.8">
<path d="M345.76 164.172H360.062C360.694 164.166 361.321 164.286 361.907 164.525C362.492 164.765 363.024 165.118 363.471 165.566C363.918 166.013 364.272 166.545 364.511 167.13C364.751 167.715 364.871 168.342 364.865 168.975V183.276C364.865 188.309 362.866 193.135 359.307 196.694C355.749 200.252 350.923 202.252 345.89 202.252H345.782C343.288 202.254 340.818 201.766 338.514 200.813C336.209 199.861 334.115 198.464 332.35 196.702C330.586 194.939 329.187 192.847 328.232 190.543C327.277 188.239 326.785 185.77 326.785 183.276C326.785 172.761 335.267 164.28 345.76 164.172Z" fill="#33B256"/>
<path d="M345.76 172.653C343.448 172.653 341.23 173.571 339.595 175.207C337.96 176.842 337.041 179.06 337.041 181.372V187.301C337.058 188.064 337.368 188.792 337.909 189.332C338.449 189.873 339.177 190.183 339.94 190.2H342.407V183.384H339.075V181.48C339.173 179.753 339.928 178.129 341.185 176.941C342.442 175.753 344.106 175.091 345.836 175.091C347.566 175.091 349.23 175.753 350.488 176.941C351.745 178.129 352.5 179.753 352.598 181.48V183.384H349.222V190.2H351.451V190.308C351.44 191.016 351.153 191.692 350.653 192.193C350.152 192.694 349.476 192.98 348.768 192.991H346.107C345.89 192.991 345.652 193.099 345.652 193.316C345.658 193.434 345.707 193.547 345.791 193.631C345.875 193.715 345.988 193.765 346.107 193.77H348.79C349.706 193.764 350.583 193.398 351.231 192.75C351.879 192.102 352.246 191.224 352.251 190.308V190.092C352.888 189.948 353.457 189.591 353.863 189.079C354.268 188.567 354.486 187.932 354.48 187.279V181.48C354.588 176.569 350.694 172.653 345.76 172.653Z" fill="white"/>
</g>
<g filter="url(#filter3_dd)">
<rect x="306.158" y="265.51" width="252.898" height="75.3313" rx="10" fill="white"/>
</g>
<path fill-rule="evenodd" clip-rule="evenodd" d="M327.023 284C334.944 284 342.935 284 350.832 284C350.832 290.93 350.832 298.052 350.832 304.982C341.545 292.883 350.832 304.982 341.545 292.883C336.704 292.883 331.841 292.883 327 292.883C327.023 290.002 327.023 284.143 327.023 284Z" fill="#00467F"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M341.522 300.862C344.626 304.791 347.591 308.864 350.648 312.818C355.512 312.818 360.398 312.818 365.262 312.818C365.262 315.771 365.262 318.748 365.262 321.701C357.341 321.701 349.443 321.701 341.522 321.701C341.522 314.747 341.522 307.816 341.522 300.862Z" fill="#00467F"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M352.336 292.836C352.336 290.455 352.336 288.168 352.336 285.787C356.088 285.787 359.84 285.787 363.569 285.787C363.569 294.265 363.569 302.768 363.569 311.246C361.091 311.246 358.589 311.246 356.111 311.246C356.111 305.125 356.111 298.933 356.111 292.836C354.884 292.836 352.359 292.836 352.336 292.836Z" fill="#B9C7D4"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M336.265 294.384C336.265 300.457 336.265 306.601 336.265 312.651C337.447 312.651 338.744 312.651 339.971 312.651C339.971 315.056 339.971 317.438 339.971 319.843C336.242 319.843 332.536 319.843 328.808 319.843C328.808 311.365 328.808 302.862 328.808 294.384C331.286 294.384 333.764 294.384 336.265 294.384Z" fill="#B9C7D4"/>
<rect x="392" y="285" width="127" height="6" rx="3" fill="#C7D0E3"/>
<circle cx="400.772" cy="315.283" r="8.51961" fill="white"/>
<circle cx="400.772" cy="315.283" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="435.746" cy="315.282" r="8.51961" fill="white"/>
<circle cx="435.746" cy="315.282" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="470.722" cy="315.282" r="8.51961" fill="white"/>
<circle cx="470.722" cy="315.282" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="400.772" cy="315.282" r="3.1388" fill="#FF3370"/>
<circle cx="470.723" cy="315.282" r="3.1388" fill="#FF3370"/>
<g filter="url(#filter4_dd)">
<rect x="306.158" y="385.681" width="287.873" height="75.3313" rx="10" fill="white"/>
</g>
<rect x="392" y="405" width="167" height="6" rx="3" fill="#C7D0E3"/>
<circle cx="400.772" cy="435.454" r="8.51961" fill="white"/>
<circle cx="400.772" cy="435.454" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="435.746" cy="435.454" r="8.51961" fill="white"/>
<circle cx="435.746" cy="435.454" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="470.722" cy="435.454" r="8.51961" fill="white"/>
<circle cx="470.722" cy="435.454" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="400.772" cy="435.454" r="3.1388" fill="#FF3370"/>
<circle cx="435.747" cy="435.454" r="3.1388" fill="#FF3370"/>
<g clip-path="url(#clip1)">
<path d="M357.612 416.635V410.838C358.635 410.243 359.33 409.146 359.33 407.877C359.33 405.978 357.792 404.439 355.893 404.439C353.994 404.439 352.455 405.978 352.455 407.877C352.455 409.146 353.15 410.243 354.174 410.838V416.635C352.868 416.875 351.654 417.377 350.596 418.095L334.315 408.791C334.368 408.494 334.407 408.19 334.407 407.877C334.407 405.028 332.097 402.72 329.25 402.72C326.404 402.72 324.094 405.028 324.094 407.877C324.094 410.725 326.404 413.033 329.25 413.033C330.536 413.033 331.694 412.547 332.597 411.77L348.074 420.612C347.043 422.126 346.439 423.954 346.439 425.925C346.439 427.914 347.057 429.759 348.106 431.282L342.162 437.226C341.017 436.923 339.748 437.206 338.852 438.105C337.509 439.448 337.509 441.623 338.852 442.966C340.195 444.309 342.37 444.309 343.713 442.966C344.611 442.068 344.895 440.802 344.593 439.657L350.536 433.712C352.059 434.761 353.903 435.379 355.893 435.379C361.115 435.379 365.347 431.147 365.347 425.925C365.347 421.29 362.011 417.444 357.612 416.635ZM355.893 431.941C352.57 431.941 349.877 429.248 349.877 425.925C349.877 422.602 352.57 419.909 355.893 419.909C359.217 419.909 361.909 422.602 361.909 425.925C361.909 429.248 359.217 431.941 355.893 431.941Z" fill="#F76245"/>
</g>
<circle cx="400.771" cy="74.9395" r="8.51961" fill="white"/>
<circle cx="400.771" cy="74.9395" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="435.745" cy="74.9395" r="8.51961" fill="white"/>
<circle cx="435.745" cy="74.9395" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="470.721" cy="74.9395" r="8.51961" fill="white"/>
<circle cx="470.721" cy="74.9395" r="8.01961" stroke="#15377C" stroke-opacity="0.2"/>
<circle cx="400.771" cy="74.9401" r="3.1388" fill="#FF3370"/>
<defs>
<filter id="filter0_d" x="0" y="186.257" width="201.424" height="201.424" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset/>
<feGaussianBlur stdDeviation="10"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0823529 0 0 0 0 0.215686 0 0 0 0 0.486275 0 0 0 0.15 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
<filter id="filter1_dd" x="271.158" y="0.166992" width="318.414" height="145.331" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="10"/>
<feGaussianBlur stdDeviation="17.5"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.901961 0 0 0 0 0.933333 0 0 0 0 0.996078 0 0 0 1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dx="4" dy="5"/>
<feGaussianBlur stdDeviation="4"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0823529 0 0 0 0 0.215686 0 0 0 0 0.486275 0 0 0 0.12 0"/>
<feBlend mode="normal" in2="effect1_dropShadow" result="effect2_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow" result="shape"/>
</filter>
<filter id="filter2_dd" x="271.158" y="120.338" width="366.841" height="145.331" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="10"/>
<feGaussianBlur stdDeviation="17.5"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.901961 0 0 0 0 0.933333 0 0 0 0 0.996078 0 0 0 1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dx="4" dy="5"/>
<feGaussianBlur stdDeviation="4"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0823529 0 0 0 0 0.215686 0 0 0 0 0.486275 0 0 0 0.12 0"/>
<feBlend mode="normal" in2="effect1_dropShadow" result="effect2_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow" result="shape"/>
</filter>
<filter id="filter3_dd" x="271.158" y="240.51" width="322.898" height="145.331" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="10"/>
<feGaussianBlur stdDeviation="17.5"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.901961 0 0 0 0 0.933333 0 0 0 0 0.996078 0 0 0 1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dx="4" dy="5"/>
<feGaussianBlur stdDeviation="4"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0823529 0 0 0 0 0.215686 0 0 0 0 0.486275 0 0 0 0.12 0"/>
<feBlend mode="normal" in2="effect1_dropShadow" result="effect2_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow" result="shape"/>
</filter>
<filter id="filter4_dd" x="271.158" y="360.681" width="357.873" height="145.331" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="10"/>
<feGaussianBlur stdDeviation="17.5"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.901961 0 0 0 0 0.933333 0 0 0 0 0.996078 0 0 0 1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dx="4" dy="5"/>
<feGaussianBlur stdDeviation="4"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.0823529 0 0 0 0 0.215686 0 0 0 0 0.486275 0 0 0 0.12 0"/>
<feBlend mode="normal" in2="effect1_dropShadow" result="effect2_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow" result="shape"/>
</filter>
<clipPath id="clip0">
<rect width="42.1496" height="42.1496" fill="white" transform="translate(325.889 42.2061)"/>
</clipPath>
<clipPath id="clip1">
<rect width="41.2528" height="41.2528" fill="white" transform="translate(324.094 402.72)"/>
</clipPath>
</defs>



  <circle r="4" fill="#FF3370" class="my-circle" >
 <animateMotion dur="3s" repeatCount="indefinite"
 	attributeName="opacity"
      path="M180 245 
  	H218 
  	C220.069,244.883  228.069,245.883 232.993,220.999 
  	V80 
  	C232.023,80.9355 233.069,60.8899 265.069,60.935 H305"/>
  </circle> 


  <circle r="4" fill="#FF3370" class="my-circle1">
 <animateMotion dur="3s" repeatCount="indefinite"
      path="M180 275 
    	H240  	
     	 C245.069,275.883 260.069,273.993 260.993,250.999 V200 
     	 C260.069,200.883 260.069,183.993 280.993,183.999 H305" begin="1s"/>
  </circle> 


  <circle r="4" fill="#FF3370" class="my-circle2">
 <animateMotion dur="3s" repeatCount="indefinite"
      path="M170 334 
    	H240 
    	C250.069,334.883 261.069,338.883 260.993,360.999 v50
    	C260.069,420.883 276.069,425.883 280.993,424.999 H300" begin="2s"/>
  </circle> 

    <circle r="4" fill="#FF3370" class="my-circle">
 <animateMotion dur="3s" repeatCount="indefinite"
      path="M190.137 304.121L300.895 304.121" begin="0s"/>
  </circle> 

<style>
	.my-circle{
	animation: hideshow 3s infinite ease-in-out;
	animation-delay:0s;
}

	.my-circle1{
	animation: hideshow 3s infinite ease-in-out;
	animation-delay:1s;
}

	.my-circle2{
	animation: hideshow 3s infinite ease-in-out;
	animation-delay:2s;
}

@keyframes hideshow {
 0% {opacity:0;}
  10%{opacity:1;}
  70% {opacity:1}
  100%{opacity:0}
} 
</style>
</svg>