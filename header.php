<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package linqanalytics
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'linqanalytics' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="container col">
			<div class="row flex-row">
				<div class="col m6 s7">
					<div class="col-left logo site-branding">
						<?php
							$header_logo = get_field('header_logo', 'option');
						?>
						<a href="/"><img src="<?php echo $header_logo; ?>" alt="Link analytics"></a>
					</div><!-- .site-branding -->
				</div>
				<div class="col m6 s5 right-align">
					<div class="col-right">
						<div class="menu-icon">
							<span></span><span></span>
                                    <span></span>						
                                </div>
					
							<nav id="site-navigation" class="main-navigation">
								<?php
								wp_nav_menu(
									array(
										'theme_location' => 'menu-1',
										'menu_id'        => 'primary-menu',
									)
								);
								?>
								<div class="show-on-small demomobbtn">
									<a class="site-btn site-btn-pink" href="/demo/">demo</a>
								</div>
							</nav><!-- #site-navigation -->
					
					</div>	
				</div>
			</div>	
		</div>
	</header><!-- #masthead -->
